﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }

        public ICollection<Reports> Reports { get; set; }
        public Customer customer { get; set; }
    }
}
