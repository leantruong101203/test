﻿using Microsoft.EntityFrameworkCore;

namespace CodeFirst.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
options) : base(options)
        {
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Reports> reports { get; set; }
        public DbSet<Logs> logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.transactions)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.TransactionalID);
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Accounts)
                .WithMany(t => t.Reports)
                .HasForeignKey(r => r.AccountID);
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.logs)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.LogsID);

            modelBuilder.Entity<Logs>()
                .HasOne(r => r.Transactions)
                .WithMany(t => t.Logs)
                .HasForeignKey(r => r.TransationalId);

            modelBuilder.Entity<Transactions>()
                .HasOne(r => r.employees)
                .WithMany(t => t.transactions)
                .HasForeignKey(r => r.EmployeeId);
        }

    }
}
