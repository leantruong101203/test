﻿namespace CodeFirst.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactandAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public ICollection<Transactions> transactions { get; set; }
        public ICollection<Accounts> Accounts { get; set; }
    }
}
