﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeId { get; set; }
        public int FirstName { get; set; }
        public int LastName { get; set; }
        public string ContactandAddress { get; set; }
        public string UserNameanfPassword { get; set; }
        public ICollection<Transactions> transactions { get; set; }
    }
}
