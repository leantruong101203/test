﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Logs
    {

        [Key]
        public int LogId { get; set; }
        public int TransationalId { get; set; }
        public string LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        public Transactions Transactions { get; set; }
        public ICollection<Reports> reports { get; set; }
    }
}
