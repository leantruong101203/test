﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Reports
    {
        [Key]
        public int ReportId { get; set; }
        public int AccountID { get; set; }
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public string ReportName { get; set; }
        public DateTime Reportdate { get; set; }
        public Transactions transactions { get; set; }
        public Logs logs { get; set; }
        public Accounts Accounts { get; set; }
    }
}
