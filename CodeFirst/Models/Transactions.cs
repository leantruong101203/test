﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalId { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public Customer customer { get; set; }
        public Employees employees { get; set; }
        public ICollection<Reports> reports { get; set; }
        public ICollection<Logs> Logs { get; set; }
    }
}
